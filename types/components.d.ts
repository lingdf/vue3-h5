// generated by unplugin-vue-components
// We suggest you to commit this file into source control
// Read more: https://github.com/vuejs/core/pull/3399
import '@vue/runtime-core'

export {}

declare module '@vue/runtime-core' {
  export interface GlobalComponents {
    GaodeMap: typeof import('./../src/components/map/gaodeMap.vue')['default']
    Map: typeof import('./../src/components/map/map.vue')['default']
    RouterLink: typeof import('vue-router')['RouterLink']
    RouterView: typeof import('vue-router')['RouterView']
    VanButton: typeof import('vant/es')['Button']
    VanField: typeof import('vant/es')['Field']
    VanIcon: typeof import('vant/es')['Icon']
    VanImage: typeof import('vant/es')['Image']
    VanPopup: typeof import('vant/es')['Popup']
    VanSpace: typeof import('vant/es')['Space']
    VenueList: typeof import('./../src/components/venueList/VenueList.vue')['default']
  }
}
