/*
 * @Date: 2022-10-08 14:15:33
 * @LastEditors: Mr.qin
 * @LastEditTime: 2022-11-11 11:28:08
 * @Description: 类型声明
 */
// interface Venue {
// 	name: string;
// 	longitude: number;
// 	latitude: number;
// 	location: string;
// 	description: string;
// 	like: boolean;
// }

interface Venue {
	id: string;
	repositoryNum: number;
	name: string;
	geocodedCode: string;
	synopsis: string;
	url: string;
	path?: any;
	password?: string;
	isOpen: number;
	isUsePassword?: any;
	isRelease: number;
	location: string;
	latitude: string;
	longitude: string;
	contact: string;
	phone: string;
	likeNum: number;
}
interface Comment {
	path;
	auditStatus: number;
	avatar: string;
	comment: string;
	createtime: string;
	id: number;
	modifytime: string;
	pavilionId: number;
	pavilionName: string;
	score: number;
	status: number;
	time: string;
	username: string;
}
