/*
 * @Date: 2022-10-08 09:19:25
 * @LastEditors: Mr.qin
 * @LastEditTime: 2022-10-14 17:46:05
 * @Description:
 */
/// <reference types="vite/client" />

declare module '*.vue' {
	import type { DefineComponent } from 'vue';
	const component: DefineComponent<{}, {}, any>;
	export default component;
}
/// <reference types="vite/client" />
interface ImportMeta {
	env: {
		GITHUB_AUTH_TOKEN: string;
		NODE_ENV: 'development' | 'test' | 'production';
		PORT?: string;
		PWD: string;
	};
}
