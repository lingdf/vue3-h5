/*
 * @Date: 2022-10-08 09:47:05
 * @LastEditors: Mr.qin
 * @LastEditTime: 2022-10-08 09:47:49
 * @Description: postcss插件配置
 */
export default {
	plugins: {
		'postcss-px-to-viewport': {
			viewportWidth: 375,
		},
	},
};
