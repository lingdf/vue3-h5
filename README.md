<!--
 * @Date: 2022-09-30 09:35:09
 * @LastEditors: Mr.qin
 * @LastEditTime: 2022-11-13 13:31:14
 * @Description: 描述文件
-->
# Vue 3 + TypeScript + Vite + Vant3

## ⭕Task

## ✨插件和库

- vant :轻量、可靠的移动端组件库
- vue-router :官方路由
- pinia :新一代状态存储库
- axios :异步请求库
- less :一门CSS扩展语言
- windicss :新颖的原子css库
- postcss : css转换插件
- nprogress :轻量级的加载进度条组件

- unplugin-auto-import :自动导入api
- unplugin-vue-components :自动导入组件
